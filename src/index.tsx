import React, { ReactElement } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { Header } from './app/components/Header/Header';
import { About } from './app/pages/about/About';
import { Experience } from './app/pages/experience/Experience';
import { Skills } from './app/pages/skills/Skills';

import './styles/app.css';
import './styles/page.css';

const App = (): ReactElement => {
	return (
		<Router basename="/cv">
			<Header></Header>
			<div className="page">
				<Route exact path="/" component={About}></Route>
				<Route exact path="/about" component={About}></Route>
				<Route exact path="/experience" component={Experience}></Route>
				<Route exact path="/skills" component={Skills}></Route>
			</div>
		</Router>
	);
};

ReactDOM.render(<App />, document.getElementById('root'));
