import React from 'react'
import { shallow } from 'enzyme'
import { Header } from './Header'

describe('Header', () => {
    it('render', () => {
        const component = shallow(<Header></Header>)
        expect(component.find('div').text()).toEqual('Header')
    })
})