import React, { ReactElement, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';

import './Header.css';

export const Header: React.FC = (): ReactElement => {
	const location = useLocation();
	useEffect(() => {
		console.log(location);
	});

	return (
		<div className="header">
			<div className="header-title">
				<div>Sam Radage</div>
				<div className="header-tagline">Developer</div>
			</div>
			<div className="header-items">
				<Link
					to="/about"
					className={
						location.pathname === '/' ||
						location.pathname === '/about'
							? 'header-item --selected'
							: 'header-item'
					}
				>
					About
				</Link>
				<Link
					to="/experience"
					className={
						location.pathname === '/experience'
							? 'header-item --selected'
							: 'header-item'
					}
				>
					Experience
				</Link>
				<Link
					to="/skills"
					className={
						location.pathname === '/skills'
							? 'header-item --selected'
							: 'header-item'
					}
				>
					Skills
				</Link>
			</div>
		</div>
	);
};
