import React, { ReactElement } from 'react';

import './Experience.css';

export const Experience = (): ReactElement => {
	const handleScroll = (element: React.UIEvent<HTMLElement>) => {
		const experienceContent = document.querySelector(
			'.experience-content'
		) as HTMLDivElement;
		const targetHeight = experienceContent.scrollHeight * 0.25;

		const yearElement = document.querySelector(
			'#year-2021'
		) as HTMLDivElement;

		const catchmentHeight = yearElement.scrollHeight / 2;
		const experienceContainer = document.querySelector(
			'#experience'
		) as HTMLDivElement;

		const year2020 = document.querySelector('#year-2020') as HTMLDivElement;
		const fromTop2020 =
			(year2020.getClientRects()[0].top -
				experienceContainer.getClientRects()[0].top) /
			catchmentHeight;
		if (fromTop2020 > 1) {
			experienceContent.scrollTop = 0;
			document
				.querySelector('.experience-year.--selected')
				?.classList.remove('--selected');
			document.querySelector('#year-2021')?.classList.add('--selected');
		}
		if (fromTop2020 <= 1 && fromTop2020 >= 0) {
			experienceContent.scrollTop = targetHeight * (1 - fromTop2020);
			document
				.querySelector('.experience-year.--selected')
				?.classList.remove('--selected');
			year2020.classList.add('--selected');
		}

		const year2018 = document.querySelector('#year-2018') as HTMLDivElement;
		const fromTop2018 =
			(year2018.getClientRects()[0].top -
				experienceContainer.getClientRects()[0].top) /
			catchmentHeight;
		if (fromTop2018 > 1 && fromTop2020 < 0) {
			experienceContent.scrollTop = targetHeight;
			document
				.querySelector('.experience-year.--selected')
				?.classList.remove('--selected');
			document.querySelector('#year-2020')?.classList.add('--selected');
		}
		if (fromTop2018 <= 1 && fromTop2018 >= 0) {
			experienceContent.scrollTop =
				targetHeight * (1 - fromTop2018) + targetHeight;
			document
				.querySelector('.experience-year.--selected')
				?.classList.remove('--selected');
			year2018.classList.add('--selected');
		}

		const year2017 = document.querySelector('#year-2017') as HTMLDivElement;
		const fromTop2017 =
			(year2017.getClientRects()[0].top -
				experienceContainer.getClientRects()[0].top) /
			catchmentHeight;
		if (fromTop2017 > 1 && fromTop2018 < 0) {
			experienceContent.scrollTop = targetHeight * 2;
			document
				.querySelector('.experience-year.--selected')
				?.classList.remove('--selected');
			year2018.classList.add('--selected');
		}
		if (fromTop2017 <= 1 && fromTop2017 >= 0) {
			experienceContent.scrollTop =
				targetHeight * (1 - fromTop2017) + targetHeight * 2;
			document
				.querySelector('.experience-year.--selected')
				?.classList.remove('--selected');
			year2017.classList.add('--selected');
		}
		if (fromTop2017 <= 0) {
			const experienceScroll = document.querySelector(
				'.experience-scroll'
			) as HTMLDivElement;
			experienceScroll.scrollTop = catchmentHeight * 8;
		}
		if (fromTop2017 < 0) {
			experienceContent.scrollTop = targetHeight * 3;
		}
	};
	return (
		<div id="experience" className="experience">
			<div className="experience-scroll" onScroll={handleScroll}>
				<div className="experience-years">
					<div className="experience-years-items">
						<div
							id="year-2021"
							className="experience-year --selected"
						>
							2021
						</div>
						<div id="year-2020" className="experience-year">
							2020
						</div>
						<div className="experience-year --spacer">2019</div>
						<div id="year-2018" className="experience-year">
							2018
						</div>
						<div id="year-2017" className="experience-year">
							2017
						</div>
						<div className="experience-year --spacer">2016</div>
						<div className="experience-year --spacer">2015</div>
						<div className="experience-year --spacer">2014</div>
					</div>
				</div>
				<div className="experience-container">
					<div className="experience-content">
						<div className="experience-item">
							<div className="experience-title">
								<div className="experience-title-year">
									2018 - Present
								</div>
								<div className="experience-title-text">
									Senior Consultant
								</div>
								<div className="experience-title-location">
									Risk Analytics - Deloitte - London
								</div>
							</div>
							<div className="experience-text">
								<div className="experience-text-sub">
									April 2021 - June 2021 : React Developer -
									UK Investment Management Company
								</div>
								<div className="experience-text-bullets">
									<ul>
										<li>
											Technologies used:{' '}
											<b>
												React, Redux, Jest, Cypress,
												TypeScript
											</b>
										</li>
										<li>
											Worked as a contractor to help
											develop a frontend for a set of
											investment models.
										</li>
										<li>
											The frontend allowed users input
											parameters, run a model and view the
											model output.
										</li>
										<li>
											Gained experience with stakeholder
											management, interacting with end
											users and project leads to agree on
											a suitable design / functionality
											for the front end.
										</li>
										<li>
											Responsible for the frontend,
											including all design, test and build
										</li>
										<li>
											Practiced a Behaviour driven
											development model
										</li>
									</ul>
								</div>
							</div>
							<div className="experience-text">
								<div className="experience-text-sub">
									September 2020 - April 2021 : Full Stack
									Developer - Investment Monitoring Platform
								</div>
								<div className="experience-text-bullets">
									<ul>
										<li>
											Technologies used:{' '}
											<b>
												Python, AWS, Puppeteer,
												JavaScript, HTML, SQL
											</b>
										</li>
										<li>
											Worked as the lead front end
											developer on a internal Deloitte
											project building an Trade
											Surveillance App.
										</li>
										<li>
											This app helps out clients (Fund
											Managers) comply with industry
											regulations by highlighting areas of
											suspicious trading activity such as
											Insider Trading.
										</li>
										<li>
											My main responsibilities included
											design and build of the front end,
											design and build of python APIs, SQL
											query design, and client support.
										</li>
										<li>
											I was also involved with some of the
											DevOps tasks such as implementing
											automated API tests and modifying
											the build and deploy pipeline to AWS
											Lambdas.
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div className="experience-item">
							<div className="experience-title">
								<div className="experience-title-year">
									2018 - Present
								</div>
								<div className="experience-title-text">
									Senior Consultant
								</div>
								<div className="experience-title-location">
									Risk Analytics - Deloitte - London
								</div>
							</div>
							<div className="experience-text">
								<div className="experience-text-sub">
									June 2020 - September 2020 - DHSC
								</div>
								<div className="experience-text-bullets">
									<ul>
										<li>
											Helped to forecast the supply and
											demand of test kits going to uk care
											homes.
										</li>
										<li>
											Experienced working under high
											pressure situations which vastly
											improved my time and stakeholder
											managements skills.
										</li>
									</ul>
								</div>
							</div>
							<div className="experience-text">
								<div className="experience-text-sub">
									December 2018 - June 2020 - Data Migration
									Testing - UK Wealth Management Firm
								</div>
								<div className="experience-text-bullets">
									<ul>
										<li>
											Part of the team that ensured the
											successful migration of around 600k
											customer accounts representing £60B
											in assets
										</li>
										<li>
											Developed SQL scripts to
											automatically test the migration of
											Accounts, Funds and Transactions pre
											and post migration.
										</li>
										<li>
											Gained a deep understanding of SQL
											and the need for efficient code when
											working with big data
										</li>
										<li>
											Experienced working with
											stakeholders, particularly when
											communicating issues that were
											uncovered during testing
										</li>
										<li>
											Responsible for junior members of
											the team, including day to day tasks
											and code review.
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div className="experience-item">
							<div className="experience-title">
								<div className="experience-title-year">
									Summer 2018
								</div>
								<div className="experience-title-text">
									Research Intern
								</div>
								<div className="experience-title-location">
									Airbus - Filton
								</div>
							</div>
							<div className="experience-text">
								<div className="experience-text-sub">
									As part of my MSc Dissertation I was invited
									to Airbus to undertake research into the use
									of machine learning to predict / identify
									gust encountered by an aircraft during
									flight.
								</div>
								<div className="experience-text-bullets">
									<ul>
										<li>
											Used neural networks to predict the
											state of the aircraft one or more
											timesteps into the future
										</li>
										<li>
											This project gave me experience
											managing my own time and path when
											performing research as I was given
											the freedom to choose my own
											approach.
										</li>
										<li>
											Insight into performing cutting edge
											research and modifying existing
											methods to fit a new purpose.
										</li>
									</ul>
								</div>
							</div>
							<div className="experience-title">
								<div className="experience-title-year">
									2017 - 2018
								</div>
								<div className="experience-title-text">
									MSc Operational Research and Finance
								</div>
								<div className="experience-title-location">
									University of Southampton
								</div>
							</div>
							<div className="experience-text">
								<div className="experience-text-sub">
									Explored the use and development of an array
									of statistical models, with the specific
									focus on how they can be used in industry,
									graduated with distinction.
								</div>
								<div className="experience-text-bullets">
									<ul>
										<li>
											Helped develop skills related to
											working with data, such as
											cleansing, preprocessing and
											visualisation.
										</li>
										<li>
											Further development of programming
											skills with a specific focus on
											statistical modelling, exploring
											languagues such as SQL, python and
											R.
										</li>
										<li>Awarded a full scholarship.</li>
									</ul>
								</div>
							</div>
						</div>
						<div className="experience-item">
							<div className="experience-title">
								<div className="experience-title-year">
									2014 - 2017
								</div>
								<div className="experience-title-text">
									BEng Aeronautics and Astronautics
								</div>
								<div className="experience-title-location">
									University of Southampton
								</div>
							</div>
							<div className="experience-text">
								<div className="experience-text-sub">
									Aeronautical Engineering with a focus on
									Aerodynamics, graduated with 1st class
									honours.
								</div>
								<div className="experience-text-bullets">
									<ul>
										<li>
											Gained an experience working with
											complex aircraft systems
										</li>
										<li>
											Helped to develop a solid background
											in Mathematics
										</li>
										<li>
											Improved team working and management
											skills over a range of group
											projects, such as the design and
											build of a UAV aircraft
										</li>
										<li>
											Introduction to programming with
											languages such as python and MATLAB,
											with a specific focus on
											mathematical simulation
										</li>
										<li>
											Developed independent research
											skills through a Research Project
											investigating the Aerodynamic
											Performance of Dragonfly Wings
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};
