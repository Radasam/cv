import React, { ReactElement } from 'react';

import './About.css';

export const About = (): ReactElement => {
	return (
		<div className="about">
			<div>
				<div className="page-title">About me</div>
				<p>
					I am a London based full stack developer with a keen
					interest in the use of technology to help to improve and
					automate our day to day lives. This interest lead me to
					learn how to code so I could experiment with my own ideas
					and make my day to day work easier.
				</p>
				<p>
					I am very passionate about software development and I enjoy
					using my skills to solve problems and improve monotonous
					processes. Hence I have focused my skills towards web
					technologies as this provides an avenue to share my work
					with others.
				</p>
				<p>
					I also have a strong belief in the use DevOps and of cloud
					services, these tools make the life of a developer easier
					and allow new features to reach the users in a much shorter
					period of time. I take a great deal of satisfaction being
					able to take an idea from conception to implementation which
					lead me to develop skills in this area.
				</p>
				<p>
					In addition to the above I use my spare time to practice
					guitar and train as a powerlifter.
				</p>
			</div>
		</div>
	);
};
