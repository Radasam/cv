import React, { ReactElement, useEffect, useState } from 'react';
import Chart from 'react-apexcharts';
import AWS from '../../../assets/AWS.png';

import './Skills.css';

export const Skills = (): ReactElement => {
	const [selectedItem, setSelectedItem] = useState<number>(0);
	const [imageDimention, setImageDimention] = useState<number>(200);

	useEffect(() => {
		if (window.innerWidth < 600) {
			setImageDimention(50);
		}
	});

	type SkillItem = {
		label: string;
		description1: string;
		description2: string;
		description3: string;
		color: string;
		light: boolean;
		experience: number;
		hasImage: boolean;
	};

	const skillItems: SkillItem[] = [
		{
			label: 'JavaScript/TypeScript',
			description1:
				'Notable packages include; Webpack, ApexCharts, Jest, Puppeteer, Cypress, Redux, Express',
			description2:
				'In a commercial setting I have used JavaScript/TypeScript to build a API service connecting a frontend application to an MSSQL database. I also have experience using JavaScript to build an end to end testing framework using Puppeteer.',
			description3:
				'I have also used JavaScript to develop some personal projects in my spare time, notable examples include a JavaScript and SQL based Database Migration testing framework, and a package to convert and export HTML pages to a readable PDF format.',
			color: '#efd81d',
			light: true,
			experience: 3,
			hasImage: false,
		},
		{
			label: 'React',
			description1:
				'React is my prefered framework for building web apps, I have used React for a wide range of commercial and personal projects spanning from a front end for investment model platform, a kanban/ticket board, a twitter clone and a testing dashboard.',
			description2:
				'I have experience with all the current React best practices, including functional components with Reack hooks, Redux for state management, React Router for URL routing, Jest and Cypress for unit and end to end testing and webpack and babel for bundling and compiling',
			description3:
				'In addition to developing web apps in React, I also have experience building and deploying React apps to cloud services such as AWS',
			color: '#7ee0fc',
			light: true,
			experience: 2,
			hasImage: false,
		},
		{
			label: 'SQL',
			description1:
				'I have extensive experience work with SQL based databases such as MSSQL and postgres SQL.',
			description2:
				'A great deal of my experience is focused on writing comparison queries testing the accuracy of data pre and post migration, which includes applying complex transformations to a large dataset of millions of records.',
			description3:
				'I have been able to apply this experience when developing backend services for my web apps, allowing me to write clear, concise and efficient queries.',
			color: '#31648c',
			light: false,
			experience: 3,
			hasImage: false,
		},
		{
			label: 'Python',
			description1:
				'I was introduced to Python during university where I was mainly focused on statistical and mathematical problems, using packages such as scikit-learn, matplotlib and tensorflow to perform various data science based tasks.',
			description2:
				'Since then I have developed these skills further to performing more backend orientated task, such as interaction with databases to create, update and fetch data.',
			description3:
				'Commercially I have experience with python developing API services using FastApi, using this package I have implemented a prediction service for a VaR model and a REST api for trade surveillance platform.',
			color: '#3c924e',
			light: false,
			experience: 4,
			hasImage: false,
		},
		{
			label: 'AWS',
			description1:
				'I have experience working with AWS cloud services, specifically from a DevOps standpoint having recently completed a certification for AWS certified DevOps Engineer - Professional',
			description2:
				'Using this experience I have successfully setup a CodePipeline project to build, test and deploy a React webapp, additionally I have also deployed a Python REST api service through Lambdas and API Gateway.',
			description3:
				'I am most comfortable interating with AWS through the infrastructure as code tools such as CloudFormation and Terraform.',
			color: '#f27416',
			light: true,
			experience: 2,
			hasImage: true,
		},
		{
			label: 'Docker',
			description1:
				'I also have experience using docker to containerise apps for development and deployment, using tools such as docker-compose to replicate a multi-container application in a local environment.',
			description2:
				'Using docker I have experience builing and deploying containers serving React webapps, express api services and Python api services',
			description3: '',
			color: '#49008c',
			light: false,
			experience: 2,
			hasImage: false,
		},
		{
			label: 'Other',
			description1:
				'Some other technologies that I have experiemented with, but are yet to use in a commercial setting include; MongoDB, Jenkins, Gatsby.',
			description2:
				'I have also encountered some other technologies during research which I hope to pick up in the future such as GraphQL to build efficient APIs and React Native to build Android and IOS apps using React code.',
			description3: '',
			color: '#a50028',
			light: false,
			experience: 3,
			hasImage: false,
		},
	];

	const handleSelect = (index: number) => {
		ApexCharts.exec('doughnut-chart', 'toggleDataPointSelection', index);
		document
			.querySelector('.apexcharts-datalabels.--selected')
			?.classList.remove('--selected');
		document
			.querySelectorAll('.apexcharts-datalabels')
			[index]?.classList.add('--selected');
		setSelectedItem(index);
	};

	const state = {
		options: {
			chart: {
				toolbar: { show: false },
				id: 'doughnut-chart',
				events: {
					dataPointMouseEnter: (
						event: any,
						chartContext: any,
						config: any
					) => {
						setSelectedItem(config.dataPointIndex);
						document
							.querySelector('.apexcharts-datalabels.--selected')
							?.classList.remove('--selected');
						document
							.querySelectorAll('.apexcharts-datalabels')
							[config.dataPointIndex]?.classList.add(
								'--selected'
							);
						setSelectedItem(config.dataPointIndex);
					},
				},
			},
			plotOptions: { pie: { expandOnClick: false } },
			labels: skillItems.map((item) => item.label),
			legend: { show: false },
			colors: skillItems.map((item) => item.color),
			tooltip: { enabled: false },
			dataLabels: {
				enabled: true,
				enabledOnSeries: [2],
				formatter: function (
					value: any,
					{ seriesIndex, dataPointIndex, w }: any
				) {
					return `${skillItems[seriesIndex].experience} years`;
				},
			},
		},

		series: skillItems.map((item) => item.experience),
	};

	return (
		<div className="skills">
			<div className="skills-plot">
				<Chart
					options={state.options}
					series={state.series}
					type={'donut'}
				></Chart>
			</div>
			<div className="skills-content">
				<div className="skills-content-tabs">
					{skillItems.map((item, index) => {
						return (
							<div
								onMouseEnter={() => handleSelect(index)}
								key={item.label}
								id={`tab-${item.label}`}
								className="skills-content-tab"
								style={{
									borderColor:
										index === selectedItem
											? 'white'
											: item.color,
									backgroundColor:
										index === selectedItem
											? item.color
											: 'white',
									color:
										index === selectedItem && !item.light
											? 'white'
											: 'black',
								}}
							>
								{item.label}
							</div>
						);
					})}
				</div>
				{skillItems[selectedItem].hasImage && imageDimention < 200 && (
					<div className="skills-image">
						<img
							src={AWS}
							height={imageDimention}
							width={imageDimention}
						/>
					</div>
				)}
				<div className="skills-content-tab-content">
					<div>
						<div className="skills-content-experience">
							<div>
								{skillItems[selectedItem].experience} years
								experience
							</div>
						</div>
						<div className="skills-content-text">
							{skillItems[selectedItem].description1}
						</div>
						<div className="skills-content-text">
							{skillItems[selectedItem].description2}
						</div>
						<div className="skills-content-text">
							{skillItems[selectedItem].description3}
						</div>
					</div>
					<div style={{ flexGrow: 1 }}></div>
					{skillItems[selectedItem].hasImage &&
						imageDimention == 200 && (
							<div className="skills-image">
								<img
									src={AWS}
									height={imageDimention}
									width={imageDimention}
								/>
							</div>
						)}
				</div>
			</div>
		</div>
	);
};
