import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps';


Given(/I am on the home page/, () => {
    cy.visit('/')
})

Then(/I should be able to see the title/, () => {
    cy.get('div').children('div').should('have.text','Header')
})